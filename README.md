# README

## Purpose

This repository contains the code used to perform splicing analysis on TCGA read junction count data.

Version 1.0


## Set up

### System
This was designed and tested on Ubuntu 14 and 16 with python 2.7, March 2018.


### Dependencies
To run the code, you will need the following python packages (install using pip or similar)

* pyreference
* scikits.statsmodels
* scipy
* seaborn
* HTSeq
* matplotlib
* numpy
* pandas

### Preparation

1. Obtain the gene annotation file for hg19 from Illumina iGenomes (`genes.gtf`).

1. Pre-process the gene annotation file using a pyreference package script to create much smaller json version:
`pyreference_gtf_to_json.py genes.gtf`

1. Modify the file pyreference.cfg file in this repository to point to the json gtf file you just produced. `build` should remain `hg19`.

1. Place this config file in your home directory.

1. Download and unpack junction count data and gene expression data from the Broad Institute website [Firehose] (https://gdac.broadinstitute.org/)

1. Decompress the data. 

1. Locate the following files: one file containing junction read count data and one containing normalised gene expression data from RSEM.

For example:

* for the junction count data for BrCa, the file I used was:
`gdac.broadinstitute.org_BRCA.Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__junction_quantification__data.Level_3.2016012800.0.0/BRCA.rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__junction_quantification__data.data.txt`
* For the gene expression data from RSEM, the file I used was:
`gdac.broadinstitute.org_BRCA.Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes_normalized__data.Level_3.2016012800.0.0/BRCA.rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes_normalized__data.data.txt`

##Run instructions

To perform comparison of QKI expression levels with splicing patterns from TCGA data:

For each cancer analysed, there is an analysis script starting with the TCGA abbreviation (e.g. `brca_tcga_splicing_analysis.py` for Breast Cancer):

* `blca_tcga_splicing_analysis.py`
* `brca_tcga_splicing_analysis.py`
* `kipan_tcga_splicing_analysis.py`
* `luad_tcga_splicing_analysis.py`
* `lusc_tcga_splicing_analysis.py`
* `prad_tcga_splicing_analysis.py`
* `stes_tcga_splicing_analysis.py`

For each script, set 3 variables at the top to supply the following paths/files:

* output directory (`outdir` variable = set as desired)
* Firehose gene expression data file (RSEM) (`expression_firehose_table_file` variable = the `*RSEM_genes_normalized__data.data.txt` file)
* Firehose junctions table (`firehose_junc_table_file` = the `*junction_quantification__data.data.txt` file)

Once this is set up, run python scripts, one at a time. They use 1 processor and, in my hands, use between ~6 GB and ~17 GB of memory each. The run time will be in the order of 12 hrs per script.

## Output
The output directory will contain two directories and an image:

* `figs-QKI` : this will contain figures comparing QKI expression to splicing levels for significant loci
* `raw_data` : this contains intermediate files that are saved in case part of the pipeline needs to be rerun.
* `QKI_high_and_low_expression_X_samples.png` : a plot of the samples with highest and lowest QKI expression - the difference between these groups should be large or it will be difficult to detect QKI-correlated changes in splicing.

### In the figs-QKI directory:
There are 3 subfolders which each contain graphs of QKI expression vs splicing for a given locus: 

* `best` - The splicing loci with graphs in this directory are likely to be the most interesting - they have the largest changes in splicing and the highest statistical significance.
* `ok` - The splicing loci with graphs in this directory passed the statistical cutoff (`MIN_PVALUE`) and minimum change in splicing (`MIN_CHANGE_IN_SPLICING`) cutoff.
* `change_too_small` - The splicing loci with graphs in this directory passed the statistical cutoff (`MIN_PVALUE`) but the splicing change (`MIN_CHANGE_IN_SPLICING`) is below the cutoff and is therefore too small to be biologically interesting.

**Interpreting the graphs:**

* Each graph shows the expression of QKI along the x-axis vs the splicing levels for a particular junction (Percent spliced along junction A (vs all other potential splicing options which share an end) along the y-axis. 
* Graphs may be either 5’->3’ or 3’->5’ (indicated as ‘start’ and ‘end’ in the graph title, respectively). 
* Each sample is represented by a single point and the error bars show the confidence of the splicing result (i.e. the expression level - for low expression level genes, count noise will have a larger impact on the accuracy of splicing metrics calculated).

** The figs-QKI directory also contains 3 files:**

* `locus_stats.tsv` - this is the locus-level data. It contains the mean splicing levels (PSI) for the groups of samples with low and high QKI expression levels, and p-values for this difference.
* `all_gene_stats.tsv` - this contains the gene-level stats, meaning for each gene, the locus with the most statistically significant correlation between splicing and QKI expression has been selected. Statistics associated with this QKI-correlated change in splicing are also shown. The locus with the largest difference in splicing (‘largest diff’) is also shown. 
* `settings.log` - this is a record of the settings used to produce the graphs.

Further information about the methods and output produced here can be found in our paper (Pillman et al. (2018) EMBOJ).

### Contact ###

* For information or if you have questions, contact Katherine Pillman (katherine(dot)pillman(at)unisa(dot)edu(dot)au)
