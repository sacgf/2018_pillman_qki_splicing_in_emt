#!/usr/bin/env python
'''
Created on 19/07/2016

@author: kpillman
'''
from os import path
from pyreference.reference import Reference

import pandas as pd
from tcga_splice_junction_analysis import get_gene_info_for_junctions, chop_up_junc_count_files, \
    calculate_junction_proportions_and_sums, get_tcga_countcols, analyse_correlation_between_goi_expression_and_splicing, \
    graph_high_and_low_qki_expression, get_qki_expression_from_firehose_table_and_save, \
    get_firehose_junction_counts_into_df
from pyreference.utils.file_utils import mk_path

if __name__ == '__main__':
    
    outdir = '/home/kpillman/localwork/gregory_splicing/tcga/blca'
    firehose_junc_table_file = '/data/sacgf/public_data/TCGA_firehose/stddata__2016_01_28/BLCA/20160128/gdac.broadinstitute.org_BLCA.Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__junction_quantification__data.Level_3.2016012800.0.0/BLCA.rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__junction_quantification__data.data.txt' 
    expression_firehose_table_file = '/data/sacgf/public_data/TCGA_firehose/stddata__2016_01_28/BLCA/20160128/gdac.broadinstitute.org_BLCA.Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes_normalized__data.Level_3.2016012800.0.0/BLCA.rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes_normalized__data.data.txt'
        
    #Parameters
    REGULARISATION_VALUE = 0.1
    ALPHA = 0.05
    MIN_COVERAGE_FOR_END_POS = 10 #Minimum total count level for a junction to be included/considered 'expressed'
    MIN_NUM_SAMPLES_FOR_ANALYSIS = 100 #Minimum number of samples a junction must be expressed in before it is skipped
    PERCENT_CUTOFF_FOR_ALL_IN_ONE_ISOFORM = 0.08 #8%
    
    TEN_PERCENT_CAP = 40 #Fewer samples so lowered this from 50
    MIN_CHANGE_IN_SPLICING = 0.1 #Splicing changes below this level are not considered interesting. Range: 0-1.
    GOOD_CHANGE_IN_SPLICING = 0.4
    MIN_PVALUE = 0.0001
    GOOD_PVALUE = 0.00001
    
    print_graphs = True
    
    raw_data_dir = path.join(outdir, 'raw_data')
    junc_counts_file = path.join(raw_data_dir, 'junc_counts.tsv')
    analysis_files_dir = path.join(raw_data_dir, 'split_analysis_files')
    junc_count_files_dir = path.join(raw_data_dir, 'split_junc_count_files')
    gene_of_interest_expression_file_regex = path.join(raw_data_dir, '%s_expression.tsv')
    qki_expression_file = path.join(raw_data_dir, gene_of_interest_expression_file_regex % 'QKI')
    generic_split_junc_count_filename = 'junction_counts-genes_starting_with_%s.tsv'
    
    gene_of_interest_names_and_gene_type = {'QKI': 'gene'}
    graph_units = {'gene': 'FPKM'}

    #Setup
    pd.set_option("display.large_repr", "info")
    mk_path(outdir)
    mk_path(raw_data_dir)
    reference = Reference()
    
    ### Step 1: Process junctions to calculate proportions and sums ###
    get_firehose_junction_counts_into_df([firehose_junc_table_file], junc_counts_file)
       
    get_gene_info_for_junctions(reference, junc_counts_file)
    chop_up_junc_count_files(junc_counts_file, junc_count_files_dir, generic_split_junc_count_filename)
    calculate_junction_proportions_and_sums(analysis_files_dir, junc_count_files_dir, generic_split_junc_count_filename, get_tcga_countcols, REGULARISATION_VALUE, MIN_COVERAGE_FOR_END_POS, PERCENT_CUTOFF_FOR_ALL_IN_ONE_ISOFORM)
        
    get_qki_expression_from_firehose_table_and_save(expression_firehose_table_file, qki_expression_file, gene_of_interest_names_and_gene_type, gene_of_interest_expression_file_regex)
    
    ### Step 2. Analyse splicing and plot graphs###
    analyse_correlation_between_goi_expression_and_splicing(outdir, analysis_files_dir, print_graphs, gene_of_interest_expression_file_regex, generic_split_junc_count_filename, gene_of_interest_names_and_gene_type, MIN_PVALUE, MIN_CHANGE_IN_SPLICING, TEN_PERCENT_CAP, MIN_COVERAGE_FOR_END_POS, MIN_NUM_SAMPLES_FOR_ANALYSIS, ALPHA, GOOD_CHANGE_IN_SPLICING, graph_units, GOOD_PVALUE=GOOD_PVALUE, mark_groups=False)
    
    graph_high_and_low_qki_expression(outdir, gene_of_interest_expression_file_regex)
    