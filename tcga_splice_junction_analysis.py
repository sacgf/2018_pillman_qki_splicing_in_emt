#!/usr/bin/env python
'''
Created on 25/08/2014
@author: katherine
Look for correlations between counts of reads across splice junctions and QKI expression.
Script designed for TCGA splicing data analysis

'''
import HTSeq
from collections import defaultdict, Counter
import glob
from os import path
from pyreference.utils.file_utils import mk_path
import re
from scikits.statsmodels.stats import multicomp
from scipy.stats import stats
import sys

import matplotlib.pyplot as pl
import numpy as np
import pandas as pd
import seaborn as sns

def tcga_get_sample_name_from_file(infile, file_sample_map_series):
    '''Returns: Sample name = patient id & sample type code (tumour, normal, etc)
    from TCGA barcode.'''
    tcga_barcode = file_sample_map_series[path.basename(infile)]
    assert tcga_barcode.startswith('TCGA')
    return tcga_barcode[:15] #Excludes Vial code 

def firehose_tcga_gene_expr_table_to_df(firehose_table_file):
    #Drop the second weird header
    df = pd.read_table(firehose_table_file, engine='python', header=0, skiprows=[1], index_col=0)
    #Rename columns to exclude Vial Code from TCGA barcode 
    df.columns = [c[:15] for c in df.columns] #Excludes Vial code 
    
    return df
    
def get_one_gene_expression_for_firehose_tcga_table(gene_id, firehose_table_file):
    '''Returns: pd.Series of expression data for given gene, index=sample_names
    '''
    df = firehose_tcga_gene_expr_table_to_df(firehose_table_file)
    
    gene_index = [i for i in df.index if i.startswith(gene_id)]
    assert len(gene_index) == 1
    gene_expression = df.loc[gene_index[0], :].copy()
    gene_expression.name=gene_id
    
    return gene_expression

def firehose_tcga_junc_table_to_df(firehose_junc_table_file):
    
    df = pd.read_table(firehose_junc_table_file, engine='python', header=0)
        
    #Drop the second weird header
    df = df[df['Hybridization REF'] != 'junction']
    
    #Files sometimes have duplicate junction entries?! But they used to seem to always have the same count values
    #Must deal with this, choose the max value.
    #Groupby takes too long so this is a hack to identify and replace dupes.    
    for junc_str, num_copies in Counter(df['Hybridization REF']).iteritems():
        if num_copies > 1: #Duplicate junctions
            replacement_row = df[df['Hybridization REF'] == junc_str].max()
            #Drop the duplicate rows and replace with single row
            df = df[df['Hybridization REF'] != junc_str]
            df = df.append(replacement_row, ignore_index=True)
    
    df['junction'] = ''
    for index, junc_str in df['Hybridization REF'].iteritems():
        
        chrom1, start_str, strand1, chrom2, end, strand2 = re.split(':|,', junc_str)
        
        #Only want junc on same chrom and strand
        if (chrom1 == chrom2) & (strand1 == strand2):
            start = int(start_str) - 1 #To match reference junction format
            end = int(end)
            
            locus = "%s:%d-%d,%s" % (chrom1, start, end, strand1)
            df.set_value(index, 'junction', locus)
    
    #Reset index
    df = df.set_index('junction', drop=True, append=False, verify_integrity=True)
    #Drop old junction column
    df = df.drop('Hybridization REF', axis=1)
    
    #Rename columns to exclude Vial Code from TCGA barcode 
    df.columns = [c[:15] for c in df.columns] #Excludes Vial code 
    
    return df
    
def get_tcga_countcols(df):
    '''Get the names of the columns with counts in them''' 
    return [c for c in df.columns if c.startswith('TCGA')]

def locus_string_to_iv(string_iv):
    '''Convert string format iv to HTSeq.GenomicInterval'''
    chrom, coord, strand = re.split(':|,', string_iv)
    start, end = coord.split('-')
    return HTSeq.GenomicInterval(chrom, int(start), int(end), strand)

def calculate_variation(which_end, junction_sums, samples_to_plot, expressed_junction_ratios):
    '''This is based on calculations done by Andreas and Kat.
    The Poisson-based variation depended only on the count noise from the total read count and the 
    % of reads across the junction (and was independant of the number of reads across that particular junction).
    junction_sums should be a dict or series with values being the sum of all the reads start/end at a given junction end.
    ratio_value is the % of those reads that span a particular junction'''
    total_variations = pd.Series(index=samples_to_plot)
    
    for sample_id in samples_to_plot:
        sum_value = junction_sums["%s-%s_sum" % (sample_id, which_end)]
        ratio_value = expressed_junction_ratios["%s-%s_prop" % (sample_id, which_end)]        
        #Calculate the amount of variation which comes from count noise
        ratio_variation = np.sqrt(((ratio_value ** 2.0) / sum_value) * (1.0 - ratio_value))
        total_variations[sample_id] = ratio_variation
    
    return total_variations

def update_stats(df, gene_name, p_value, diff_between_y_means, locus):
    diff_between_y_means = abs(diff_between_y_means) #discard direction of change
    
    if not gene_name in df.index:
        df = df.append(pd.Series({'p_value':p_value, 'diff_between_means':diff_between_y_means, 'num_graphs_printed':0, 'best_locus':locus, 'largest_diff':diff_between_y_means, 'pvalue_for_largest_diff':p_value}, name=gene_name))
        
    else: #keep the p-value and diff for the lowest p-value
        if diff_between_y_means > df.loc[gene_name, 'diff_between_means']:
            df.set_value(gene_name, 'largest_diff', diff_between_y_means)
            df.set_value(gene_name, 'pvalue_for_largest_diff', p_value)
            
        if p_value < df.loc[gene_name, 'p_value']:  
            df.set_value(gene_name, 'p_value', p_value)
            df.set_value(gene_name, 'diff_between_means', diff_between_y_means)
            df.set_value(gene_name, 'best_locus', locus)
            
    return df

def get_q_values(all_pvalues, ALPHA):
    _, all_q_values, _, _ = multicomp.multipletests(all_pvalues, ALPHA, 'fdr_bh')
    q_values_for_p_values = {}
    for p, q in zip(all_pvalues, all_q_values):
        q_values_for_p_values[p] = q
        
    return q_values_for_p_values

def get_firehose_junction_counts_into_df(firehose_junc_table_files, junc_counts_file):
    '''The order of the tables in firehose_junc_table_files indicates where to get data from if a sample is in both: earlier files takes precedence over later ones'''
    print "Reading in junction counts"
    final_df = pd.DataFrame()
    for i, firehose_junc_table_file in enumerate(firehose_junc_table_files):
        df = firehose_tcga_junc_table_to_df(firehose_junc_table_file)        
        print "Dataframe %d contains %d samples" % (i + 1, len(df.columns))
        
        new_cols = df.columns.difference(final_df.columns)
        print "Found %d new samples in dataframe %d" % (len(new_cols), i + 1)
        
        final_df = final_df.join(df.loc[:, new_cols], how='outer')
    
    print "Got junction table."        
    print final_df
    
    final_df.to_csv(junc_counts_file, sep='\t')
    return df

def get_gene_info_for_junctions(reference, junc_counts_file):
    '''Add information about which gene each junction is in.'''
    print "Getting gene info for junctions"
    df = pd.read_table(junc_counts_file, index_col=0, engine='python')
    print df
    
    rename_indexes_with_strand = {}
    df['genes'] = ''
    df['num_genes'] = np.NaN
    df['chrom'] = ''
    df['start'] = np.NaN
    df['end'] = np.NaN
    df['strand'] = ''
    
    for a, (locus, _) in enumerate(df.iterrows()):
        junc_iv = locus_string_to_iv(locus)
        gene_name = "None"
        num_genes = 0
                    
        #Where there is a perfectly matching junction in the gtf, use this.
        if junc_iv.strand == '.': #Where no strand provided, collect transcripts on both strands.
            junc_iv.strand = '+' #dummy
            transcripts = reference.get_transcripts_in_unstranded_iv(junc_iv) #unstranded
            junc_iv.strand = '.' # put back
            
            for transcript in transcripts:
                intron_ivs = transcript.get_intron_ivs()
                #Junctions are 1 bp either side of introns
                candidate_junc_ivs = [HTSeq.GenomicInterval(ii.chrom, ii.start - 1, ii.end + 1, '.') for ii in intron_ivs]
                  
                if junc_iv in set(candidate_junc_ivs):
                    gene_name = transcript.get_gene_id()
                    junc_iv.strand = transcript.iv.strand #Assign strand from transcript
                    num_genes = 1
                    rename_indexes_with_strand[locus] = "%s:%d-%d,%s" % (junc_iv.chrom, junc_iv.start, junc_iv.end, junc_iv.strand)
                    break
            
        else:
            transcripts = reference.get_transcripts_in_iv(junc_iv) #stranded
            
            for transcript in transcripts:
                intron_ivs = transcript.get_intron_ivs()
                #Junctions are 1 bp either side of introns
                candidate_junc_ivs = [HTSeq.GenomicInterval(ii.chrom, ii.start - 1, ii.end + 1, ii.strand) for ii in intron_ivs]
                  
                if junc_iv in set(candidate_junc_ivs):
                    gene_name = transcript.get_gene_id()
                    num_genes = 1
                    break
                
        #Otherwise, consider overlapping genes:
        if gene_name == "None":
            gene_names = set([t.get_gene_id() for t in transcripts])
            
            #where there is only one gene overlapping, use that
            if len(gene_names) == 1:
                gene_name, = gene_names
                if junc_iv.strand == '.': #Assign strand from gene
                    strands = set([t.iv.strand for t in transcripts])
                    junc_iv.strand, = strands
                    rename_indexes_with_strand[locus] = "%s:%d-%d,%s" % (junc_iv.chrom, junc_iv.start, junc_iv.end, junc_iv.strand)
                num_genes = 1
            
            elif len(gene_names) > 1:
                #Otherwise, if there is more than overlapping gene on this strand, use both.
                #If you have to do this, flag the gene as a difficult one so it can be skipped later.
                #Note: should this be all the genes or only the ones on this strand?
                gene_name = ','.join(gene_names)
                num_genes = len(gene_names)
            
            #Note: at the moment, only genes on the appropriate strand are considered.
            #genes on the other strand are ignored.
    #         else: #Can't assign to a gene
    #             pass
        
        df.set_value(locus, 'genes', gene_name)
        df.set_value(locus, 'num_genes', num_genes)
        df.set_value(locus, 'chrom', junc_iv.chrom)
        df.set_value(locus, 'start', junc_iv.start)
        df.set_value(locus, 'end', junc_iv.end)
        df.set_value(locus, 'strand', junc_iv.strand)
        
        if not (a % 50000):
            print a
    
    df.rename(index=rename_indexes_with_strand)
    
    df.to_csv(junc_counts_file, sep='\t')
    print 'Finished writing to file'
    
def chop_up_junc_count_files(junc_counts_file, junc_count_files_dir, generic_split_junc_count_filename):
    '''Reading in the junc_counts_file uses up a huge amount of memory (~15GB) when using engine=python.
    Therefore ideally rewrite previous steps to chop up earlier.'''
    ### Chop up count file into bite-sized files for processing efficiency###
    #Split by first letter of gene name, but this choice was arbitrary, just wanted to split into ~20 files.
    print "Chopping up files"
    df = pd.read_table(junc_counts_file, index_col=0, engine='python')
    print df
    mk_path(junc_count_files_dir)
    
    locus_gene_first_letter = defaultdict(list)
    for locus, row in df.iterrows():
        gene_name_first_letter = str(row['genes'][0])
        
        if gene_name_first_letter == '1': #Ridiculously, one gene name starts with 1. Lump this with the 'A's.
            gene_name_first_letter = 'A'
        
        #One gene starts with a lowercase letter: tAKR. 
        #Having files that are identical except for lower or upper case breaks across mount point so lump tAKR with 'T's.
        gene_name_first_letter = gene_name_first_letter.upper()  
        
        locus_gene_first_letter[gene_name_first_letter].append(locus)
    
    for gene_name_first_letter, loci in locus_gene_first_letter.iteritems():
        first_letter_df = df.ix[loci]
        first_letter_df.to_csv(path.join(junc_count_files_dir, generic_split_junc_count_filename % gene_name_first_letter), sep='\t')
     


def get_qki_expression_from_firehose_table(firehose_table_file):    
    return get_one_gene_expression_for_firehose_tcga_table('QKI', firehose_table_file)
   
def get_qki_expression_from_firehose_table_and_save(firehose_table_file, qki_expression_file, gene_of_interest_names_and_gene_type, gene_of_interest_expression_file_regex):
    '''Get qki expression and save to file'''    
    print "Getting QKI expression data"
    qki_expression = get_qki_expression_from_firehose_table(firehose_table_file)
    print qki_expression.head()
    qki_expression.to_csv(qki_expression_file, sep='\t')
    
    ### Get gene of interest expression data and save to file ###
    qki_expression = pd.read_table(qki_expression_file, squeeze=True, index_col=0, header=None, engine='python')
    
    for gene_of_interest_name, gene_type in gene_of_interest_names_and_gene_type.iteritems():
        print gene_of_interest_name
        if gene_type == 'gene':
            try:
                gene_of_interest_expression = get_one_gene_expression_for_firehose_tcga_table(gene_of_interest_name, firehose_table_file)
            except ValueError as detail:
                print detail
                continue
        else:
            sys.exit("Error, currently, 'gene' is the only implemented gene_type")
             
        gene_of_interest_expression.to_csv(gene_of_interest_expression_file_regex % gene_of_interest_name, sep='\t')
        
def calculate_junction_proportions_and_sums(analysis_files_dir, junc_count_files_dir, generic_split_junc_count_filename, get_count_cols, REGULARISATION_VALUE, MIN_COVERAGE_FOR_END_POS, PERCENT_CUTOFF_FOR_ALL_IN_ONE_ISOFORM):
    ### Calculate proportions and sums for start and end sites and add to spreadsheet ###
    print "Calculating junction proportions and sums"
    mk_path(analysis_files_dir)
    split_junc_count_files = glob.glob(path.join(junc_count_files_dir, generic_split_junc_count_filename % '*'))
    #For each split count file
    for split_junc_count_file in sorted(split_junc_count_files): #, reverse=True):
        print "Working on", split_junc_count_file
                   
        df = pd.read_table(split_junc_count_file, index_col=0, engine='python')
                         
        count_cols = get_count_cols(df)
                          
        ### Regularise the data - Add a small pseudocount to the counts to prevent errors relating to dividing by zero later ###
        df[count_cols] = df[count_cols] + REGULARISATION_VALUE
        
        #Identify junctions with only one gene associated - only do analyses for these
        junc_with_1_gene_df = df[df['num_genes'] == 1]
                         
        #add empty columns to spreadsheet
        for which_end in ['start', 'end']: #separate spreadsheets for start and end (i.e. 5' and 3' junction ends)
            one_end_df = df.copy() #Meaning 'start' or 'end'
                             
            #Later, we want a way to skip junctions which are not differentially spliced
            one_end_df['is_diff_spliced'] = 'unknown' #so whole column dtype=str
            one_end_df['do_not_test_splicing'] = False #so whole column dtype=bool
            one_end_df['not_expressed'] = False #so whole column dtype=bool
            
            for sample_count_col in count_cols:
                one_end_df['%s-%s_prop' % (sample_count_col, which_end)] = np.NaN
                one_end_df['%s-%s_sum' % (sample_count_col, which_end)] = np.NaN
                       
            #For each gene
            b = 0
            for gene_name, gene_data in junc_with_1_gene_df.groupby('genes'):
                                  
                if not (b % 50): # Progress monitor
                    print b, which_end, gene_name
                b += 1
                                      
                # Group the junctions by start or end position.    
                for _, end_grouped_df in gene_data.groupby(which_end):
                    loci_for_an_end = end_grouped_df.index
                               
                    #Note: Must exclude non-count data or result of sum() is autoformatted as object/str() which results in extremely poor performance.
                    end_grouped_counts = end_grouped_df[count_cols]
                         
                    #Fill in whether this junc is differentially spliced or not:
                    if len(end_grouped_counts) > 1:
                        one_end_df.loc[loci_for_an_end, 'is_diff_spliced'] = 'yes'
                    else:
                        one_end_df.loc[loci_for_an_end, 'is_diff_spliced'] = 'no'
                         
                    # For each group and sample, calculate the % of the reads that cross each junction in that group, 
                    #as a % of the total number of reads in that group in that sample.  
                    total_for_an_end_group = end_grouped_counts.sum(axis=0)
                    
                    for sample_count_col, total_for_an_end_group_for_sample in total_for_an_end_group.iteritems():
                        #Store the sum for all samples in this start group                    
                        one_end_df.loc[loci_for_an_end, "%s-%s_sum" % (sample_count_col, which_end)] = total_for_an_end_group_for_sample 
                         
                        if total_for_an_end_group_for_sample > (REGULARISATION_VALUE * len(loci_for_an_end)): #Check that this transcript or part of the transcript is expressed in this sample
                            
                            #Proportions for all loci in this start group
                            end_grouped_counts_for_sample = end_grouped_df[sample_count_col]                        
                            end_grouped_data_for_sample_as_proportion = end_grouped_counts_for_sample / float(total_for_an_end_group_for_sample)
                            
                            #If there was more than one locus with this start/end pos, this will be a dataframe with the proportions for both (e.g. 0.8 and 0.2)
                            #Therefore iterate through the dataframe and store each value
                            for end_locus, end_proportion in end_grouped_data_for_sample_as_proportion.iteritems():
                                one_end_df.set_value(end_locus, '%s-%s_prop' % (sample_count_col, which_end), end_proportion)

                    # Don't want to do statistical test on samples where vast majority of reads are in one isoform in all samples.
                    for locus_for_an_end in loci_for_an_end:
                        count_data_for_locus_for_an_end = one_end_df.loc[locus_for_an_end, count_cols] 
                        
                        if count_data_for_locus_for_an_end.max() < MIN_COVERAGE_FOR_END_POS: #Mark loci with very low expression in all samples, for easy filtering out later
                            one_end_df.set_value(locus_for_an_end, 'not_expressed', True)
                        
                        prop_cols_for_samples = ["%s-%s_prop" % (cc, which_end) for cc in count_cols]
                        prop_data_for_locus_for_an_end = one_end_df.loc[locus_for_an_end, prop_cols_for_samples]
                        
                        if (prop_data_for_locus_for_an_end.min() > (1.0 - PERCENT_CUTOFF_FOR_ALL_IN_ONE_ISOFORM)) | (prop_data_for_locus_for_an_end.max() < PERCENT_CUTOFF_FOR_ALL_IN_ONE_ISOFORM):
                            one_end_df.set_value(locus_for_an_end, 'do_not_test_splicing', True)
                             
            one_end_df.to_csv(path.join(analysis_files_dir, "%s-%s" % (which_end, path.basename(split_junc_count_file))), sep='\t')
    print "Finished %s" % split_junc_count_file


def analyse_correlation_between_goi_expression_and_splicing(outdir, analysis_files_dir, print_graphs, gene_of_interest_expression_file_regex, generic_split_junc_count_filename, gene_of_interest_names_and_gene_type, MIN_PVALUE, MIN_CHANGE_IN_SPLICING, TEN_PERCENT_CAP, MIN_COVERAGE_FOR_END_POS, MIN_NUM_SAMPLES_FOR_ANALYSIS, ALPHA, GOOD_CHANGE_IN_SPLICING, graph_units, GOOD_PVALUE=0.00000000001, mark_groups=False):
    print "Analysing correlation between GOI and splicing levels"
    print "Printing graphs = %s" % print_graphs
    
    for x_axis_gene_name, gene_type in gene_of_interest_names_and_gene_type.iteritems():
        print x_axis_gene_name
        
        x_axis_gene_expression = pd.read_table(gene_of_interest_expression_file_regex % x_axis_gene_name, squeeze=True, index_col=0, header=None, engine='python')
        if x_axis_gene_name == 'RBM9': 
            x_axis_gene_name = 'RBFOX2' #Use more recent name
        
        #Analyse tumour codes
        tumour_codes = Counter([i[-2:] for i in x_axis_gene_expression.index])
        print "There are %d samples" % (len(x_axis_gene_expression))
        print "Number of samples for each Tumour code:", tumour_codes
        
        raw_x_axis_units = graph_units[gene_type] #'FPKM'
        graph_main_output_dir = path.join(outdir, 'figs-%s' % (x_axis_gene_name))
        mk_path(graph_main_output_dir)        
        ok_dir = path.join(graph_main_output_dir, 'ok')
        fav_dir = path.join(graph_main_output_dir, 'best')
        change_too_small_dir = path.join(graph_main_output_dir, 'change_too_small')
        mk_path(change_too_small_dir)
        mk_path(ok_dir)
        mk_path(fav_dir)
        print "There is expression data for %d samples" % len(x_axis_gene_expression)
        
        locus_stats_filename = path.join(graph_main_output_dir, 'locus_stats.tsv')
        locus_stats_df = pd.DataFrame(columns=['gene'] + ['mean-QKI_low', 'mean-QKI_high'])
        
        #This means get all files for any 'end' and any letter of the alphabet. e.g. *-junction_counts-genes_starting_with_*.tsv 
        split_analysis_files_regex = path.join(analysis_files_dir, "*-%s" % (generic_split_junc_count_filename % '*'))
        split_analysis_files = glob.glob(split_analysis_files_regex)
        print "There are %d split analysis files" % len(split_analysis_files)
          
        #Get samples which have both gene expression and splicing data.    
        total_num_samples = len(x_axis_gene_expression)
        print "Found %d samples with gene expression and splicing data" % total_num_samples    
        
        # For each split analysis file
        #Process TCGA data and print graphs as you go
        all_genes_stats_df = pd.DataFrame(columns=['num_graphs_printed', 'p_value', 'q_value', 'diff_between_means', 'best_locus', 'largest_diff', 'pvalue_for_largest_diff'])
        all_pvalues = [] 
        genes_with_too_few_samples_expressed = set()
        for split_analysis_file in sorted(split_analysis_files):
            which_end = path.basename(split_analysis_file).split('-')[0]
            df = pd.read_table(split_analysis_file, index_col=0, engine='python')
            
            diff_spliced_juncs_df = df[df['is_diff_spliced'] == 'yes']
            diff_spliced_juncs_df = diff_spliced_juncs_df[diff_spliced_juncs_df['do_not_test_splicing'] == False] #Exclude genes where one isoform dominates in all samples
            #Note: it is extremely likely no loci will ever yet 'True' for not expressed, because there are so many samples, but keeping this filtering step in for consistency with goodall data analysis:
            diff_spliced_juncs_df = diff_spliced_juncs_df[diff_spliced_juncs_df['not_expressed'] == False] #Quick way to exclude loci which are very poorly expressed            
            diff_spliced_juncs_df = diff_spliced_juncs_df.sort_values(by='genes', axis=0)
            
            for locus, junction_data in diff_spliced_juncs_df.iterrows():
                 
                gene_name = junction_data['genes']
                graph_output_dir = graph_main_output_dir #reset
                                
                #Only do analysis on junctions which are interesting
                #Test 1: Must have more than x points
                #i.e. Biological meaning: In at least x samples, the expression of this region must be high enough for this change to be relevant.
                #This also ends up removing datapoints with large error bars
                junction_sums = junction_data[["%s-%s_sum" % (c, which_end) for c in x_axis_gene_expression.index]]
                #Identify samples with expression above a cutoff: 
                junc_sum_data_for_samples_with_junction_end_expressed = junction_sums[junction_sums >= MIN_COVERAGE_FOR_END_POS]
                # If too few samples have decent expression, skip the junction
                if len(junc_sum_data_for_samples_with_junction_end_expressed) < MIN_NUM_SAMPLES_FOR_ANALYSIS:
                    genes_with_too_few_samples_expressed.add(gene_name)
                    continue
                 
                #These are the samples which have expression above the cutoff
                samples_to_plot = [s.replace('-%s_sum' % which_end, '') for s in junc_sum_data_for_samples_with_junction_end_expressed.index]
                num_samples_to_plot = len(samples_to_plot) 
                  
                #Exclude samples with expression below a certain level
                expressed_junction_ratios = junction_data[["%s-%s_prop" % (c, which_end) for c in samples_to_plot]]
                y = expressed_junction_ratios        
                x = x_axis_gene_expression.copy()            
                x = x[samples_to_plot]
                x = np.log2(x)
                
                #Determine whether most extreme points are different using Welch's t-test.
                x_sorted = x.copy()
                x_sorted = x_sorted.sort_values()
                ten_percent_of_num_samples = int(num_samples_to_plot * 0.1)
                if ten_percent_of_num_samples > TEN_PERCENT_CAP: #Cap at top 50 samples, where there's lots of data. This is because in some cases we're missing some genes by including too many, e.g LAS1L
                    ten_percent_of_num_samples = TEN_PERCENT_CAP
                lowest_qki_expression_samples =  x_sorted[:ten_percent_of_num_samples]
                highest_qki_expression_samples =  x_sorted[(num_samples_to_plot - ten_percent_of_num_samples):]
                y_for_lowest_qki = y.ix[["%s-%s_prop" % (sample_id, which_end) for sample_id in lowest_qki_expression_samples.index]] 
                y_for_highest_qki = y.ix[["%s-%s_prop" % (sample_id, which_end) for sample_id in highest_qki_expression_samples.index]] 
                 
                _, p_value = stats.ttest_ind(y_for_lowest_qki, y_for_highest_qki, equal_var=False)
                all_pvalues.append(p_value)
                 
                #Test 2: Difference between the means must be at least x
                #i.e. how large is this change in splicing?
                y_mean_lowest_qki = np.mean(y_for_lowest_qki) 
                y_mean_highest_qki = np.mean(y_for_highest_qki)                 
                diff_between_y_means = y_mean_lowest_qki - y_mean_highest_qki #dont loose sign, you'll want it later.
                    
                #Store stats per gene.
                all_genes_stats_df = update_stats(all_genes_stats_df, gene_name, p_value, abs(diff_between_y_means), locus)        
                #Store stats per locus
                locus_end_str = "%s_%s" % (locus, which_end)
                locus_stats_df.set_value(locus_end_str, 'gene', gene_name)
                locus_stats_df.set_value(locus_end_str, 'mean-QKI_low', y_mean_lowest_qki)
                locus_stats_df.set_value(locus_end_str, 'mean-QKI_high', y_mean_highest_qki)
                locus_stats_df.set_value(locus_end_str, 'diff_bet_means-QKI_low_vs_QKI_high', diff_between_y_means)
                locus_stats_df.set_value(locus_end_str, 'pvalue-QKI_low_vs_QKI_high', p_value)
                
                #Determine whether to print graphs or not
                #Only print junctions for which splicing changes with qki expression
                if p_value > MIN_PVALUE:
        #             graph_output_dir = path.join(graph_output_dir, 'ns')
                    continue
                elif abs(diff_between_y_means) < MIN_CHANGE_IN_SPLICING:
                    graph_output_dir = change_too_small_dir
        #             continue
                elif (abs(diff_between_y_means) > GOOD_CHANGE_IN_SPLICING) & (p_value < GOOD_PVALUE):
                    graph_output_dir = fav_dir
                else:
                    graph_output_dir = ok_dir
                
                #Produce graph
                print "%s\t%s\t%g\t%g\t%s" % (gene_name, locus, p_value, abs(diff_between_y_means), path.basename(graph_output_dir))
                all_genes_stats_df.loc[gene_name, 'num_graphs_printed'] += 1
                
                filesys_safe_graph_name = "%s_%s_junction_%s-scatter.png" % (gene_name, locus.replace(':', '_').replace(',', '_'), which_end) #Req no colons or commas
                graph_filename = path.join(graph_output_dir, filesys_safe_graph_name) 
                
                if print_graphs:
                    #Don't overwrite if plot already exists
#                     if os.path.isfile(graph_filename):
#                         continue
                    
                    #Show on graph the regions and stats used for the t-test calculations
                    x_lower_boundary = lowest_qki_expression_samples[-1]
                    x_upper_boundary = highest_qki_expression_samples[0]        
                    y_stdev_lowest_qki = np.std(y_for_lowest_qki)
                    y_stdev_highest_qki = np.std(y_for_highest_qki)
                     
                    #Calculate error bar values for graph
                    total_variations = calculate_variation(which_end, junction_sums, samples_to_plot, expressed_junction_ratios)        
                    
                    normal_samples = [bc for bc in x.index if bc[-2:].startswith('1')] 
                    tumour_samples = [bc for bc in x.index if bc[-2:].startswith('0')]
                    
                    pl.figure(figsize=[7, 5])
                    for samples, color in zip([tumour_samples, normal_samples], ['#3333FF', '#3333FF']): #'#FF6600'
                        if len(samples) == 0:
                            continue
                        y_index_for_samples = ["%s-%s_prop" % (bc, which_end) for bc in samples ]                            
                        pl.errorbar(x.ix[samples], y.ix[y_index_for_samples], yerr=total_variations.ix[samples], c=color, fmt='o', alpha=0.4, ms=6)
                    
                    if mark_groups: #Display regions with highest/lowest QKI and average splicing levels on the plot
                        xmin, xmax = pl.xlim()
                        pl.errorbar([xmax - 0.2, xmax - 0.2], [y_mean_lowest_qki, y_mean_highest_qki], yerr=[y_stdev_lowest_qki, y_stdev_highest_qki], c='k', fmt='o', alpha=0.6, ms=8, label='%g' % p_value)
                        pl.axvspan(xmin, x_lower_boundary, facecolor='0.5', alpha=0.1) #Region with lowest QKI samples
                        pl.axvspan(x_upper_boundary, xmax, facecolor='0.5', alpha=0.1) #Region with highest QKI samples
                        pl.xlim(xmin=xmin, xmax=xmax)
                    
                    pl.title('%s, dbm:%.2f, p-value:%g, n=%d' % (gene_name, abs(diff_between_y_means), p_value, num_samples_to_plot))
                    pl.xlabel('%s expression (Log2 %s)' % (x_axis_gene_name, raw_x_axis_units))
                    pl.ylabel('Proportion of fragments')
                                            
                    pl.yticks([0, 0.5, 1])
                    pl.ylim(ymin=0, ymax=1.0)
                    
                    pl.savefig(graph_filename)
                    pl.close()
        
        #Perform multiple testing correction using the p-values from every single test, not just the interesting ones with graphs printed. 
        print "%d tests were done. Calculating q-values." % len(all_pvalues)
        q_values_for_p_values = get_q_values(all_pvalues, ALPHA)
         
        for ix in all_genes_stats_df.index:
            all_genes_stats_df.set_value(ix, 'q_value', q_values_for_p_values[all_genes_stats_df.loc[ix, 'p_value']])
         
        all_genes_stats_df.to_csv(path.join(graph_main_output_dir, 'all_genes_stats.tsv'), sep='\t')
        
        locus_stats_df.to_csv(locus_stats_filename, sep='\t')
        
        #These are the genes that had evidence of alternative splicing but were expressed too lowly to be analysed.
        genes_with_too_few_samples_expressed = genes_with_too_few_samples_expressed.difference(all_genes_stats_df.index)
        print "%d genes had evidence of alternative splicing but were not tested because no juncs had enough samples with decent expression." % len(genes_with_too_few_samples_expressed)
        
        #Print settings used to file for reference later
        print "Printing run settings to file."
        settings_fh = open(path.join(graph_main_output_dir, 'settings.log'), 'w')
        settings_fh.write('MIN_PVALUE: %g\nMIN_CHANGE_IN_SPLICING: %g\nTEN_PERCENT_CAP: %g\nMIN_COVERAGE_FOR_END_POS: %g\nMIN_NUM_SAMPLES_FOR_ANALYSIS: %g\nALPHA: %g' % (MIN_PVALUE, MIN_CHANGE_IN_SPLICING, TEN_PERCENT_CAP, MIN_COVERAGE_FOR_END_POS, MIN_NUM_SAMPLES_FOR_ANALYSIS, ALPHA))
        settings_fh.close()
        
        print "Finished analysis for %s" % x_axis_gene_name

def graph_high_and_low_qki_expression(outdir, gene_of_interest_expression_file_regex):
    '''Want to analyse the ave highest and lowest QKI expression levels in this cancer
    because these were (generally) the samples whose splicing was compared 
    (unless target gene expression was too low, then the next-best samples were used)'''
    print "Graphing QKI expression for samples with highest and lowest QKI expression"
    #Get QKI expression data
    x_axis_gene_name = 'QKI'        
    x_axis_gene_expression = pd.read_table(gene_of_interest_expression_file_regex % x_axis_gene_name, squeeze=True, index_col=0, header=None, engine='python')        
    x_axis_gene_expression = x_axis_gene_expression.sort_values()
    
    #Get data for top and bottom 10% of samples, i.e. 10%
    max_num_samples = int(len(x_axis_gene_expression) * 0.1)
    
    lowest_qki_expression_samples =  x_axis_gene_expression[:max_num_samples]
    highest_qki_expression_samples =  x_axis_gene_expression[-max_num_samples:]
    df_index = range(max_num_samples)
    lowest_qki_expression_samples.index = df_index
    highest_qki_expression_samples.index = df_index
    
    df = pd.DataFrame({'low': lowest_qki_expression_samples, 'high': highest_qki_expression_samples}, index=df_index, columns=['low', 'high'])
    df = np.log2(df)
    print df.head()
    
    pl.figure(figsize=(4,5))
    sns.set_style('ticks')
    sns.boxplot(data=df)
    pl.ylabel('%s expression (Log2 FPKM)' % x_axis_gene_name)
    pl.ylim(ymin=7, ymax=15)
    pl.savefig(path.join(outdir, '%s_high_and_low_expression_%d_samples.png' % (x_axis_gene_name, max_num_samples)))
    
#     df.to_csv(path.join(outdir, '%s_high_and_low_expression.csv' % x_axis_gene_name))

