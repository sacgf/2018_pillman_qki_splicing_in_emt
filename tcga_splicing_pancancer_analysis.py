#!/usr/bin/env python
'''
Created on 21/07/2016

@author: kpillman
'''
from os import path

import matplotlib.pyplot as pl
import pandas as pd
import seaborn as sns
from tcga_splice_junction_analysis import firehose_tcga_gene_expr_table_to_df

def file_to_array(file_name, comment=None):
    array = []
    with open(file_name) as f:
        for line in f:
            if comment and line.startswith(comment):
                continue
            array.append(line.rstrip())
    return array

#Parameters
outdir = '/home/kpillman/Desktop'
tcga_base_dir = '/data/sacgf/immun/data/analysis_project_specific/Gregory_QKI/TCGA_psi'
direct_targets_file = path.join(tcga_base_dir, 'QKI_direct_targets.txt')
firehose_gene_expr_regex = '/data/sacgf/public_data/TCGA_firehose/stddata__2016_01_28/%s/20160128/gdac.broadinstitute.org_%s.Merge_rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes_normalized__data.Level_3.2016012800.0.0/%s.rnaseqv2__illuminahiseq_rnaseqv2__unc_edu__Level_3__RSEM_genes_normalized__data.data.txt' 
locus_stats_file_regex = path.join(tcga_base_dir, "%s", "figs-QKI", "locus_stats.tsv")
 
DIFF_CUTOFF = 0.1
LARGEST_DIFF_CUTOFF = 0.35
min_expr_cutoff = 600.0
method = 'weighted'
metric = 'cityblock'

cancers_pvalue_cutoff = {'brca': 0.00001, 
                         'prad': 0.0001,
                         'lusc': 0.0001,
                        'luad': 0.0001,
                        'blca': 0.001,
                        'kipan': 0.00001,
                        'stes': 0.001,
                         }
 
#Set up
pd.set_option("display.large_repr", "info")

#Read in PSI data from all cancers
psi_for_cancers = {}
df = pd.DataFrame()
for cancer, pvalue_cutoff in cancers_pvalue_cutoff.iteritems():
    print cancer, pvalue_cutoff
    locus_stats_df = pd.read_table(locus_stats_file_regex % cancer, index_col=0)
     
    #Filter by p-value
    sig_loci_df = locus_stats_df[locus_stats_df['pvalue-QKI_low_vs_QKI_high'] < pvalue_cutoff].copy()
     
    sig_loci_df['diff_bet_means-QKI_low_vs_QKI_high'] = abs(sig_loci_df['diff_bet_means-QKI_low_vs_QKI_high'])
     
    #Get largest change in PSI for genes passing p-value cutoff
    best_locus_diff = sig_loci_df.groupby('gene')['diff_bet_means-QKI_low_vs_QKI_high'].max()
     
    #Diff larger than cutoff too
    best_locus_diff = best_locus_diff[best_locus_diff > DIFF_CUTOFF]
     
    psi_for_cancers[cancer] = best_locus_diff.copy()
 
#Collate into 1 dataframe
df = pd.DataFrame(psi_for_cancers)
df = df.fillna(0)
print df
 
#Exclude genes where diff is not greater than x for at least 1 cancer
largest_diff = df.max(axis=1)
print largest_diff.head()
df = df.loc[largest_diff > LARGEST_DIFF_CUTOFF, :]
print df.head()
 
#Filter by only genes that are EXPRESSED in all cancers.
#Would expect this would give much more consistent splicing results.
#Otherwise, expect that lots of the differences observed are due to tissue-specific expression.
print "Get constitutively expressed genes"
cancers_median_gene_expressions = []
for cancer in cancers_pvalue_cutoff.iterkeys():
    
    cancer = cancer.upper()
    
    firehose_gene_expr_file = firehose_gene_expr_regex % (cancer, cancer, cancer)
    temp_df = firehose_tcga_gene_expr_table_to_df(firehose_gene_expr_file)
    
    #Get the average (median) gene expression for every gene
    cancer_median_gene_expressions = temp_df.median(axis=1)
    cancer_median_gene_expressions.name = cancer
    cancers_median_gene_expressions.append(cancer_median_gene_expressions)
    
#Combine the data for all cancers
gene_expr_df = pd.concat(cancers_median_gene_expressions, axis=1)
print gene_expr_df

#Filter to require expression above a cutoff for ALL cancers.
gene_expr_df = gene_expr_df.loc[gene_expr_df.min(axis=1) >= min_expr_cutoff, :]
print "There are %d genes that pass the cutoff in all cancers" % len(gene_expr_df)
print gene_expr_df.head()
 
shortened_index = [i.split('|')[0] for i in gene_expr_df.index if (not i.startswith('?'))]
constit_expr_df = df.loc[[i for i in shortened_index if i in df.index], :]
print constit_expr_df.head()

### Produce heatmap ###
cmap = sns.cubehelix_palette(as_cmap=True, start=2.8, rot=0.1, light=1) #Blue-purple

clustermap = sns.clustermap(constit_expr_df, figsize=(9,12), cmap=cmap, col_cluster=False, metric=metric, yticklabels=False, method=method)
pl.savefig(path.join(outdir, '%s_heatmap.png' % method))
pl.close()

